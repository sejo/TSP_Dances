// Based on the example by
// Daniel Shiffman
// Nature of Code: Intelligence and Learning
// https://github.com/shiffman/NOC-S17-2-Intelligence-Learning

// Code for this video: https://youtu.be/9Xy-LMAfglE
//

// JSON of poses
var poses;

var posesNames = [];

// Stores the distances between poses
var distances = [];


// Array of cities
var cities = [];
var totalPoses;

// Best total distance
var recordDistance;
// Best path
var bestEver;

// An order of cities
var order = [];

// How many total possibilities
var totalPermutations;
// How many have we checked?
var count = 1;

// How far along are we?
var progress;

function preload(){
  poses = loadJSON('data/Poses1.json');

}

function setup() {
  var canvas = createCanvas(800, 400);
  canvas.parent('canvas');
  progress = select('#progress');


	// Create map of distances between poses
  calculateDistances();

	// Initialize the order
  var i = 0;
  for(var pose in poses){
    order[i] = i;
    posesNames[i] = pose;
    i++;
  }

  totalPoses = i;



  // Calculate total distance through array
  var d = calcDistance(order);
  recordDistance = d;
	// Save order
  bestEver = order.slice();
  console.log(d);

  // Total possibilities
  totalPermutations = factorial(totalPoses);
}

function draw() {
  background(255);




  // Try a new possibility
  var d = calcDistance(order);
  if (d < recordDistance) {
    recordDistance = d;
    bestEver = order.slice();
  }
  console.log(bestEver);
  printBestEverOrder();
  console.log(recordDistance);

  textSize(21);
  fill(0);
  text("Current order (Distance: "+d+")",10,40);
  text("Best order (Distance: "+recordDistance+")",10,200);

  noFill();
  // Draw current order
  for(var i=0; i<order.length;i++){
    drawPose(posesNames[order[i]],10+50*2.5*i,50,50);
  }

  // Draw bestEver order
  for(var i=0; i<order.length;i++){
    drawPose(posesNames[bestEver[i]],10+50*2.5*i,210,50);
  }

  // How far along?
  textSize(15);
  fill(255);
  var percent = 100 * (count / totalPermutations);
  progress.html(nf(percent, 0, 2));
  nextOrder();
}


function drawPose(name,x,y,s){
	var joints = poses[name];

	rect(x,y,2.5*s,2.5*s);

	push();

	translate(x+s,y+s);
	fill(255,0,0);
	noStroke();
	scale(s);
	for(var j in joints){
		ellipse(joints[j].x,-joints[j].y,0.1,0.1);
	}
	pop();


}

// Create table of distances between poses
function calculateDistances(){
	var dist;
	var j1, j2;
	var v1, v2;
	// For each pose
	for(var pose in poses){
		console.log("Distances from "+pose);
		distances[pose] = [];
		// Check every other pose
		for(var pose2 in poses){
			if(pose2 != pose){
				console.log("To "+pose2);
				// Reset distance
				dist = 0;
				// And measure distance for each joint
				for(var joint in poses[pose]){
//					console.log(joint);
					j1 = poses[pose][joint];
					j2 = poses[pose2][joint];
					v1 = createVector(j1.x,j1.y,j1.z);
					v2 = createVector(j2.x,j2.y,j2.z);
					/*
					console.log(v1);
					console.log(v2);
					*/

					dist += p5.Vector.dist(v1,v2);
				}
				// Save distance
				distances[pose][pose2] = dist;
				console.log(dist);
			}

		}
	}

	console.log(distances);

}

function printBestEverOrder(){
	for(var i=0; i<order.length;i++){
		console.log(posesNames[bestEver[i]]);
	}
}

// An array swap function
function swap(a, i, j) {
  var temp = a[i];
  a[i] = a[j];
  a[j] = temp;
}

// Total distance through points based on order
function calcDistance(order) {
  var sum = 0;
  for (var i = 0; i < order.length - 1; i++) {
    var poseAIndex = order[i];
    var poseBIndex = order[i + 1];
    var d = distances[posesNames[poseAIndex]][posesNames[poseBIndex]];
    sum += d;
  }
  return sum;
}

// This is my lexical order algorithm

function nextOrder() {
  count++;

  // STEP 1 of the algorithm
  // https://www.quora.com/How-would-you-explain-an-algorithm-that-generates-permutations-using-lexicographic-ordering
  var largestI = -1;
  for (var i = 0; i < order.length - 1; i++) {
    if (order[i] < order[i + 1]) {
      largestI = i;
    }
  }
  if (largestI == -1) {
    noLoop();
    console.log('finished');
  }

  // STEP 2
  var largestJ = -1;
  for (var j = 0; j < order.length; j++) {
    if (order[largestI] < order[j]) {
      largestJ = j;
    }
  }

  // STEP 3
  swap(order, largestI, largestJ);

  // STEP 4: reverse from largestI + 1 to the end
  var endArray = order.splice(largestI + 1);
  endArray.reverse();
  order = order.concat(endArray);
}

function factorial(n) {
  if (n == 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}
