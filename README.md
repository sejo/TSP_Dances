# TSP_Dances
Searching for the choreographic sequence with the less "cost" between poses

More info [here](http://escenaconsejo.org/en/blog/itp-blog/spring-2017/the-nature-of-code-intelligence-and-learning/english-tsp-dances-exercise-1/)

The [p5](http://p5js.org) code is live [here](https://sejomagno.github.io/TSP_Dances/08_TSP_Lexical/)
