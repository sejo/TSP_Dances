/*
	Express.js Static pages example
	Shows how to serve static pages along with dynamic routes
	in Express.js 4.0

	created 10 Feb 2015
	modified 2 Nov 2015
	by Tom Igoe
*/

var express = require('express');			// include express.js
var app = express();								// a local instance of it

// serve static pages from public/ directory:
app.use('/',express.static('08_TSP_Lexical'));


// this runs after the server successfully starts:
function serverStart() {
  var port = server.address().port;
  console.log('Server listening on port '+ port);
}


// start the server:
var server = app.listen(8080, serverStart);


