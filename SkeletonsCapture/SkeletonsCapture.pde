/*
   c to capture a pose
   s to save the poses to a JSON

   */



/*
  Based on:
Thomas Sanchez Lengeling.
 http://codigogenerativo.com/
 
 KinectPV2, Kinect for Windows v2 library for processing
 
 3D Skeleton.
 Some features a not implemented, such as orientation
 */


import javax.swing.JOptionPane;

import KinectPV2.KJoint;
import KinectPV2.*;


KinectPV2 kinect;



float zVal = 30;
float rotX = PI;

JSONObject poses;

void setup() {
  size(1024, 768, P3D);

  poses = new JSONObject();



  
  kinect = new KinectPV2(this);

  kinect.enableColorImg(true);

  //enable 3d  with (x,y,z) position
  kinect.enableSkeleton3DMap(true);

  kinect.init();
  
}

void draw() {
  background(0);

  
  image(kinect.getColorImage(), 0, 0, 320, 240);

  //translate the scene to the center 
  pushMatrix();
  translate(width/2, height/2, 0);
  scale(zVal);
  rotateX(rotX);

  ArrayList<KSkeleton> skeletonArray =  kinect.getSkeleton3d();

  //individual JOINTS
  for (int i = 0; i < skeletonArray.size(); i++) {
    KSkeleton skeleton = (KSkeleton) skeletonArray.get(i);
    if (skeleton.isTracked()) {
      KJoint[] joints = skeleton.getJoints();

      //draw different color for each hand state
      drawHandState(joints[KinectPV2.JointType_HandRight]);
      drawHandState(joints[KinectPV2.JointType_HandLeft]);

      //Draw body
      color col  = skeleton.getIndexColor();
      stroke(col);
      drawBody(joints);
    }
  }
  popMatrix();
  

  fill(255, 0, 0);
  text(frameRate, 50, 50);
}


void keyPressed(){
	switch(key){
		case 's':
			// Get filename
			String filename = (String)JOptionPane.showInputDialog(null,"Write the filename");
			filename = "data/"+filename+".json";
			// Save JSON Object
			saveJSONObject(poses,filename);
			println("Saved file in "+filename+"!");
		break;
		
		case 'c':
			capturePose();
			println("Captured!");
		break;

	}

}

void capturePose(){
  KJoint[] joints = ((KSkeleton)kinect.getSkeleton3d().get(0)).getJoints();

	// Name of the pose
	String poseName = (String)JOptionPane.showInputDialog(null,"Write the name of the pose");
	println(poseName);

	// Process data
  // Save skeleton and picture (?)
  
	JSONObject pose = getPoseAsJSONObject(joints);


	// Add pose to JSON
	poses.setJSONObject(poseName,pose);

}


JSONObject getPoseAsJSONObject(KJoint[] joints){
	JSONObject pose = new JSONObject();
	JSONObject joint;

	// Add each joint as JSON Object


	// Center
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_Head);
	pose.setJSONObject("Head",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_Neck);
	pose.setJSONObject("Neck",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_SpineShoulder);
	pose.setJSONObject("SpineShoulder",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_SpineMid);
	pose.setJSONObject("SpineMid",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_SpineBase);
	pose.setJSONObject("SpineBase",joint);


	// Right 
	// Arm
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_ShoulderRight);
	pose.setJSONObject("ShoulderRight",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_ElbowRight);
	pose.setJSONObject("ElbowRight",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_WristRight);
	pose.setJSONObject("WristRight",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_HandRight);
	pose.setJSONObject("HandRight",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_HandTipRight);
	pose.setJSONObject("HandTipRight",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_ThumbRight);
	pose.setJSONObject("ThumbRight",joint);
	// Leg
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_HipRight);
	pose.setJSONObject("HipRight",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_KneeRight);
	pose.setJSONObject("KneeRight",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_AnkleRight);
	pose.setJSONObject("AnkleRight",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_FootRight);
	pose.setJSONObject("FootRight",joint);

	// Left 
	// Arm
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_ShoulderLeft);
	pose.setJSONObject("ShoulderLeft",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_ElbowLeft);
	pose.setJSONObject("ElbowLeft",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_WristLeft);
	pose.setJSONObject("WristLeft",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_HandLeft);
	pose.setJSONObject("HandLeft",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_HandTipLeft);
	pose.setJSONObject("HandTipLeft",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_ThumbLeft);
	pose.setJSONObject("ThumbLeft",joint);
	// Leg
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_HipLeft);
	pose.setJSONObject("HipLeft",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_KneeLeft);
	pose.setJSONObject("KneeLeft",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_AnkleLeft);
	pose.setJSONObject("AnkleLeft",joint);
	joint = getJointAsJSONObject(joints, KinectPV2.JointType_FootLeft);
	pose.setJSONObject("FootLeft",joint);

	return pose;
}

JSONObject getJointAsJSONObject(KJoint[] joints, int jointType){
	JSONObject joint = new JSONObject();
	joint.setFloat("x",joints[jointType].getX());
	joint.setFloat("y",joints[jointType].getY());
	joint.setFloat("z",joints[jointType].getZ());

	return joint;
}



void drawBody(KJoint[] joints) {
  drawBone(joints, KinectPV2.JointType_Head, KinectPV2.JointType_Neck);
  drawBone(joints, KinectPV2.JointType_Neck, KinectPV2.JointType_SpineShoulder);
  drawBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_SpineMid);

  drawBone(joints, KinectPV2.JointType_SpineMid, KinectPV2.JointType_SpineBase);
  drawBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderRight);
  drawBone(joints, KinectPV2.JointType_SpineShoulder, KinectPV2.JointType_ShoulderLeft);
  drawBone(joints, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipRight);
  drawBone(joints, KinectPV2.JointType_SpineBase, KinectPV2.JointType_HipLeft);

  // Right Arm    
  drawBone(joints, KinectPV2.JointType_ShoulderRight, KinectPV2.JointType_ElbowRight);
  drawBone(joints, KinectPV2.JointType_ElbowRight, KinectPV2.JointType_WristRight);
  drawBone(joints, KinectPV2.JointType_WristRight, KinectPV2.JointType_HandRight);
  drawBone(joints, KinectPV2.JointType_HandRight, KinectPV2.JointType_HandTipRight);
  drawBone(joints, KinectPV2.JointType_WristRight, KinectPV2.JointType_ThumbRight);

  // Left Arm
  drawBone(joints, KinectPV2.JointType_ShoulderLeft, KinectPV2.JointType_ElbowLeft);
  drawBone(joints, KinectPV2.JointType_ElbowLeft, KinectPV2.JointType_WristLeft);
  drawBone(joints, KinectPV2.JointType_WristLeft, KinectPV2.JointType_HandLeft);
  drawBone(joints, KinectPV2.JointType_HandLeft, KinectPV2.JointType_HandTipLeft);
  drawBone(joints, KinectPV2.JointType_WristLeft, KinectPV2.JointType_ThumbLeft);

  // Right Leg
  drawBone(joints, KinectPV2.JointType_HipRight, KinectPV2.JointType_KneeRight);
  drawBone(joints, KinectPV2.JointType_KneeRight, KinectPV2.JointType_AnkleRight);
  drawBone(joints, KinectPV2.JointType_AnkleRight, KinectPV2.JointType_FootRight);

  // Left Leg
  drawBone(joints, KinectPV2.JointType_HipLeft, KinectPV2.JointType_KneeLeft);
  drawBone(joints, KinectPV2.JointType_KneeLeft, KinectPV2.JointType_AnkleLeft);
  drawBone(joints, KinectPV2.JointType_AnkleLeft, KinectPV2.JointType_FootLeft);

  drawJoint(joints, KinectPV2.JointType_HandTipLeft);
  drawJoint(joints, KinectPV2.JointType_HandTipRight);
  drawJoint(joints, KinectPV2.JointType_FootLeft);
  drawJoint(joints, KinectPV2.JointType_FootRight);

  drawJoint(joints, KinectPV2.JointType_ThumbLeft);
  drawJoint(joints, KinectPV2.JointType_ThumbRight);

  drawJoint(joints, KinectPV2.JointType_Head);
}


void drawJoint(KJoint[] joints, int jointType) {
  strokeWeight(2.0f + joints[jointType].getZ()*8);
  point(joints[jointType].getX(), joints[jointType].getY(), joints[jointType].getZ());
}

void drawBone(KJoint[] joints, int jointType1, int jointType2) {
  strokeWeight(2.0f + joints[jointType1].getZ()*8);
  point(joints[jointType2].getX(), joints[jointType2].getY(), joints[jointType2].getZ());
}

void drawHandState(KJoint joint) {
  handState(joint.getState());
  strokeWeight(5.0f + joint.getZ()*8);
  point(joint.getX(), joint.getY(), joint.getZ());
}

void handState(int handState) {
  switch(handState) {
  case KinectPV2.HandState_Open:
    stroke(0, 255, 0);
    break;
  case KinectPV2.HandState_Closed:
    stroke(255, 0, 0);
    break;
  case KinectPV2.HandState_Lasso:
    stroke(0, 0, 255);
    break;
  case KinectPV2.HandState_NotTracked:
    stroke(100, 100, 100);
    break;
  }
}